(function (angular) {
    'use strict';
    var app = angular.module('skylar', ['ngRoute']);

    app.factory('typeOnlyOneTime', function() {
        var typeOnlyOneTime = true;
        return typeOnlyOneTime;
    });

    app.config(function ($routeProvider) {
        $routeProvider
            .when('/home', {
                templateUrl: 'views/home.html',
                controller: 'homeCtrl'
            })
            .when('/blog', {
                templateUrl: 'views/blog.html',
                controller: 'blogCtrl'
            })
            .when('/blog/:id', {
                templateUrl: 'views/blog-post.html',
                controller: 'blogPostCtrl'
            })
            .when('/support', {
                templateUrl: 'views/support.html',
                controller: 'supportCtrl'
            })
            .when('/pricing', {
                templateUrl: 'views/pricing.html',
                controller: 'pricingCtrl'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'aboutCtrl'
            })
            .when('/success', {
                templateUrl: 'views/success.html',
                controller: 'successCtrl'
            })
            .otherwise({
                redirectTo: '/home'
            });
    });

    app.controller('mainCtrl', ['$scope', function ($scope) {
        $scope.typedString = [
            " invite everyone to have <br> a meeting tomorrow at tropical",
            " Lorem ipsum dolor sit amet", " consectetur adipisicing elit. Rerum"
        ];


        $scope.typedSecond = ['Lorem ipsum dolor', 'sit amet, consectetur', 'adipisicing elit.'];
    }]);

    app.controller('homeCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('blogCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('blogPostCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('supportCtrl', ['$scope', function ($scope) {
        $scope.rateTab = 'tab1';

        $scope.setTab = function(newTab){
            $scope.rateTab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.rateTab === tabNum;
        };
    }]);

    app.controller('pricingCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('aboutCtrl', ['$scope', function ($scope) {
        $scope.initMap = function () {
            var myLatLng = {lat: 51.5073655, lng: -0.1278708};


            var stylesArray = [
                {
                    "featureType": "landscape",
                    "stylers": [{"saturation": -100}, {"lightness": 65}, {"visibility": "on"}]
                }, {
                    "featureType": "poi",
                    "stylers": [{"saturation": -100}, {"lightness": 51}, {"visibility": "simplified"}]
                }, {
                    "featureType": "road.highway",
                    "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
                }, {
                    "featureType": "road.arterial",
                    "stylers": [{"saturation": -100}, {"lightness": 30}, {"visibility": "on"}]
                }, {
                    "featureType": "road.local",
                    "stylers": [{"saturation": -100}, {"lightness": 40}, {"visibility": "on"}]
                }, {
                    "featureType": "transit",
                    "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
                }, {"featureType": "administrative.province", "stylers": [{"visibility": "off"}]}, {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [{"visibility": "on"}, {"lightness": -25}, {"saturation": -100}]
                }, {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{"hue": "#ffff00"}, {"lightness": -25}, {"saturation": -97}]
                }];

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                disableDefaultUI: true,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });

            map.setOptions({styles: stylesArray});
        };
    }]);

    app.controller('successCtrl', ['$scope', function ($scope) {

    }]);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
})(angular);






