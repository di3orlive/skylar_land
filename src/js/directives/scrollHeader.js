(function (angular) {
    'use strict';

    angular.module('skylar').directive("scrollHeader", function () {
        return {
            restrict: "A",
            compile: function (e) {
                var $menu = angular.element(e),
                    $window = angular.element(window),
                    top = $('body').offset().top;


                angular.element($window).scroll(function () {
                    var currentScroll = $(this).scrollTop();
                    if (currentScroll > top) {
                        $menu.addClass('active');
                    }else {
                        $menu.removeClass('active');
                    }
                });
            }
        };
    });

})(window.angular);