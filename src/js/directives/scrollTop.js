﻿(function (angular) {
    'use strict';

    angular.module('skylar').directive("scrollTop", function () {
        return {
            restrict: "A",
            link: function (scope, element, attr, form) {
                angular.element(window).scroll(function () {
                    if (angular.element(this).scrollTop() > 150) {
                        element.fadeIn(300);
                    } else {
                        element.fadeOut(300);
                    }
                });

                element.on("click", function () {
                    $("html, body").animate({ scrollTop: 0 }, 500);
                    return false;
                });
            }
        };
    });

})(window.angular);