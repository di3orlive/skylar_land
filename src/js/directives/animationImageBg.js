(function (angular) {
    'use strict';

    angular.module('skylar').directive("animationImagesBg", function ($interval) {
        return {
            restrict: "A",
            scope: {
                animationImagesBg: "@"
            },
            link: function (scope, element, attr, form) {

                element.find('img').on("load", function() {

                    var widthListPr = 0;
                    var imgsArr = element.find('img');
                    var elementWidth = element.width();
                    var frontDir = true;

                    $(imgsArr).each(function(){
                        widthListPr = widthListPr + $(this).width();
                    });
                    var gallSlide = widthListPr - elementWidth;


                    var moveForvard = function(){
                        element.find('.sk-about-x2-img').css({'transform': 'translateX(0px)', transition: 'all ' + scope.animationImagesBg + 's linear'});
                        element.find('.sk-about-x2-img').css({transform: 'translateX(' + -gallSlide + 'px)'});
                    };
                    var moveBack = function(){
                        element.find('.sk-about-x2-img').css({'transform': 'translateX(0px)', transition: 'all ' + scope.animationImagesBg + 's linear'});
                        element.find('.sk-about-x2-img').css({transform: 'translateX(0px)'});
                    };

                    moveForvard();

                    $interval(function(){
                        if(frontDir){
                            frontDir = false;
                            moveBack();
                        }else{
                            frontDir = true;
                            moveForvard();
                        }
                    }, 1000*scope.animationImagesBg);


                });

            }
        };
    });

})(window.angular);