﻿(function (angular) {
    'use strict';

    angular.module('skylar').directive("scrollToAnchor", function () {
        return {
            restrict: "A",
            scope: {
                scrollToAnchor: '@'
            },
            link: function (scope, element, attr, form) {
                element.on("click", function () {
                    console.log( scope.scrollToAnchor );
                    $('html, body').animate({
                        scrollTop: $('#' + scope.scrollToAnchor).offset().top - 100
                    }, 500);
                    return false;
                });
            }
        };
    });

})(window.angular);