﻿(function (angular) {
    'use strict';

    angular.module('skylar').directive("typedjs", function (typeOnlyOneTime, $timeout) {
        return {
            restrict: 'E',
            scope: {
                strings: '='
            },
            link: function($scope, $element, $attrs) {
                var options = {
                    strings: $scope.strings,
                    loop: true,
                    contentType: 'html',
                    typeSpeed: 50,
                    onStringTyped: function() {
                        $('.starter').removeClass('starter');
                    },
                    callback: function(){
                        $('.typed-cursor').remove();
                    }
                };



                if(typeOnlyOneTime){
                    typeOnlyOneTime = false;

                    $(function() {
                        $element.typed(options);
                    });
                }else{
                    $('.starter').removeClass('starter');
                    $(function() {
                        $element.typed(options);
                    });
                }
            }
        };
    });

})(window.angular);