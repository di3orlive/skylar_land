var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    notify = require("gulp-notify"),
    util = require('gulp-util'),
    sass = require('gulp-sass'),
    imageop = require('gulp-image-optimization'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    minifycss = require('gulp-minify-css'),
    fileinclude = require('gulp-file-include'),
    server = require('gulp-server-livereload'),
    autoprefixer = require('gulp-autoprefixer'),
    gutil = require('gulp-util'),
    fs = require('fs'),
    clean = require('gulp-clean'),
    wiredep = require('wiredep').stream,
    log = util.log;

gulp.task('bower', function () {
    gulp.src('./src/**/*.html')
        .pipe(wiredep({directory: './src/bower'}))
        .pipe(gulp.dest('./src/'));
    gulp.src('./src/bower/**/*.*')
        .pipe(gulp.dest('./public/bower'));
});

gulp.task('scss', function () {
    var sassErr = sass({});
    sassErr.on('error',function(e){
        gutil.log(e);
        sassErr.end();
    });

    log('============Generate CSS files============');
    gulp.src(['./src/scss/all.scss'])
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(sass())
        .pipe(concat('main.css'))
        .pipe(autoprefixer('last 3 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gulp.dest('public/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('./public/css/min'))
        .on('error', reportError);
});

gulp.task('js', function() {
    gulp.src('./public/js/**/*.js')
        .pipe(plumber({
            errorHandler: reportError
        }));

    gulp.src(['./src/js/main.js', './src/js/**/*.js'])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('public/js/'));
});

gulp.task('img-min', function(cb) {
    gulp.src('src/i/**/*.*')
        .pipe(imageop({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        })).pipe(gulp.dest('./public/i')).on('end', cb).on('error', cb);
});

gulp.task('webserver', function () {
    gulp.src('./public/')
        .pipe(server({
                livereload: true,
                port: 11111,
                open: './public/index.html',
                directoryListing: {
                    enable: true,
                    path: 'public'
                }
            })
        )
});

gulp.task('release', function () {
    var number = gutil.env.number;
    //gulp release --number 0.1
    if (fs.existsSync('./releae/' + number)){
        return console.error('Number ' + number + ' already exists')
    }
    console.log('Making release ' + number + ' ');
    gulp.src('./public/**/*.*')
        .pipe(gulp.dest("./releases/" + number + '/'));
});

gulp.task('fileinclude', function() {
    gulp.src(['./src/**/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./public/'));
});

gulp.task('watch', function () {
    log('Watching scss files for modifications');
    gulp.watch('./src/**/*.html', ['fileinclude']);
    gulp.watch('./src/scss/**/*.scss', ['scss']);
    gulp.watch('./src/js/**/*.js', ['js']);
    //gulp.watch('./src/i/**/*.*', ['img']);
    gulp.watch('bower.json', ['bower']);
});

gulp.task('default', function () {
    gulp.run('scss', 'js', 'watch', 'webserver');
});


//======================================================================================================================


var reportError = function (error) {
    notify({
        title: 'Error',
        message: 'Check the console.'
    }).write(error);

    console.log(error.toString());

    this.emit('end');
};