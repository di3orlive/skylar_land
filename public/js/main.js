(function (angular) {
    'use strict';
    var app = angular.module('skylar', ['ngRoute']);

    app.factory('typeOnlyOneTime', function() {
        var typeOnlyOneTime = true;
        return typeOnlyOneTime;
    });

    app.config(function ($routeProvider) {
        $routeProvider
            .when('/home', {
                templateUrl: 'views/home.html',
                controller: 'homeCtrl'
            })
            .when('/blog', {
                templateUrl: 'views/blog.html',
                controller: 'blogCtrl'
            })
            .when('/blog/:id', {
                templateUrl: 'views/blog-post.html',
                controller: 'blogPostCtrl'
            })
            .when('/support', {
                templateUrl: 'views/support.html',
                controller: 'supportCtrl'
            })
            .when('/pricing', {
                templateUrl: 'views/pricing.html',
                controller: 'pricingCtrl'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'aboutCtrl'
            })
            .when('/success', {
                templateUrl: 'views/success.html',
                controller: 'successCtrl'
            })
            .otherwise({
                redirectTo: '/home'
            });
    });

    app.controller('mainCtrl', ['$scope', function ($scope) {
        $scope.typedString = [
            " invite everyone to have <br> a meeting tomorrow at tropical",
            " Lorem ipsum dolor sit amet", " consectetur adipisicing elit. Rerum"
        ];


        $scope.typedSecond = ['Lorem ipsum dolor', 'sit amet, consectetur', 'adipisicing elit.'];
    }]);

    app.controller('homeCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('blogCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('blogPostCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('supportCtrl', ['$scope', function ($scope) {
        $scope.rateTab = 'tab1';

        $scope.setTab = function(newTab){
            $scope.rateTab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.rateTab === tabNum;
        };
    }]);

    app.controller('pricingCtrl', ['$scope', function ($scope) {

    }]);

    app.controller('aboutCtrl', ['$scope', function ($scope) {
        $scope.initMap = function () {
            var myLatLng = {lat: 51.5073655, lng: -0.1278708};


            var stylesArray = [
                {
                    "featureType": "landscape",
                    "stylers": [{"saturation": -100}, {"lightness": 65}, {"visibility": "on"}]
                }, {
                    "featureType": "poi",
                    "stylers": [{"saturation": -100}, {"lightness": 51}, {"visibility": "simplified"}]
                }, {
                    "featureType": "road.highway",
                    "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
                }, {
                    "featureType": "road.arterial",
                    "stylers": [{"saturation": -100}, {"lightness": 30}, {"visibility": "on"}]
                }, {
                    "featureType": "road.local",
                    "stylers": [{"saturation": -100}, {"lightness": 40}, {"visibility": "on"}]
                }, {
                    "featureType": "transit",
                    "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
                }, {"featureType": "administrative.province", "stylers": [{"visibility": "off"}]}, {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [{"visibility": "on"}, {"lightness": -25}, {"saturation": -100}]
                }, {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{"hue": "#ffff00"}, {"lightness": -25}, {"saturation": -97}]
                }];

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                disableDefaultUI: true,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });

            map.setOptions({styles: stylesArray});
        };
    }]);

    app.controller('successCtrl', ['$scope', function ($scope) {

    }]);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
})(angular);







(function (angular) {
    'use strict';

    angular.module('skylar').directive("animationImagesBg", function ($interval) {
        return {
            restrict: "A",
            scope: {
                animationImagesBg: "@"
            },
            link: function (scope, element, attr, form) {

                element.find('img').on("load", function() {

                    var widthListPr = 0;
                    var imgsArr = element.find('img');
                    var elementWidth = element.width();
                    var frontDir = true;

                    $(imgsArr).each(function(){
                        widthListPr = widthListPr + $(this).width();
                    });
                    var gallSlide = widthListPr - elementWidth;


                    var moveForvard = function(){
                        element.find('.sk-about-x2-img').css({'transform': 'translateX(0px)', transition: 'all ' + scope.animationImagesBg + 's linear'});
                        element.find('.sk-about-x2-img').css({transform: 'translateX(' + -gallSlide + 'px)'});
                    };
                    var moveBack = function(){
                        element.find('.sk-about-x2-img').css({'transform': 'translateX(0px)', transition: 'all ' + scope.animationImagesBg + 's linear'});
                        element.find('.sk-about-x2-img').css({transform: 'translateX(0px)'});
                    };

                    moveForvard();

                    $interval(function(){
                        if(frontDir){
                            frontDir = false;
                            moveBack();
                        }else{
                            frontDir = true;
                            moveForvard();
                        }
                    }, 1000*scope.animationImagesBg);


                });

            }
        };
    });

})(window.angular);
(function (angular) {
    'use strict';

    angular.module('skylar').directive("scrollHeader", function () {
        return {
            restrict: "A",
            compile: function (e) {
                var $menu = angular.element(e),
                    $window = angular.element(window),
                    top = $('body').offset().top;


                angular.element($window).scroll(function () {
                    var currentScroll = $(this).scrollTop();
                    if (currentScroll > top) {
                        $menu.addClass('active');
                    }else {
                        $menu.removeClass('active');
                    }
                });
            }
        };
    });

})(window.angular);
(function (angular) {
    'use strict';

    angular.module('skylar').directive("scrollToAnchor", function () {
        return {
            restrict: "A",
            scope: {
                scrollToAnchor: '@'
            },
            link: function (scope, element, attr, form) {
                element.on("click", function () {
                    console.log( scope.scrollToAnchor );
                    $('html, body').animate({
                        scrollTop: $('#' + scope.scrollToAnchor).offset().top - 100
                    }, 500);
                    return false;
                });
            }
        };
    });

})(window.angular);
(function (angular) {
    'use strict';

    angular.module('skylar').directive("scrollTop", function () {
        return {
            restrict: "A",
            link: function (scope, element, attr, form) {
                angular.element(window).scroll(function () {
                    if (angular.element(this).scrollTop() > 150) {
                        element.fadeIn(300);
                    } else {
                        element.fadeOut(300);
                    }
                });

                element.on("click", function () {
                    $("html, body").animate({ scrollTop: 0 }, 500);
                    return false;
                });
            }
        };
    });

})(window.angular);
(function (angular) {
    'use strict';

    angular.module('skylar').directive("typedjs", function (typeOnlyOneTime, $timeout) {
        return {
            restrict: 'E',
            scope: {
                strings: '='
            },
            link: function($scope, $element, $attrs) {
                var options = {
                    strings: $scope.strings,
                    loop: true,
                    contentType: 'html',
                    typeSpeed: 50,
                    onStringTyped: function() {
                        $('.starter').removeClass('starter');
                    },
                    callback: function(){
                        $('.typed-cursor').remove();
                    }
                };



                if(typeOnlyOneTime){
                    typeOnlyOneTime = false;

                    $(function() {
                        $element.typed(options);
                    });
                }else{
                    $('.starter').removeClass('starter');
                    $(function() {
                        $element.typed(options);
                    });
                }
            }
        };
    });

})(window.angular);